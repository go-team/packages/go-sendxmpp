module salsa.debian.org/mdosch/go-sendxmpp

go 1.22.0

toolchain go1.22.7

require (
	github.com/ProtonMail/gopenpgp/v2 v2.8.2
	github.com/beevik/etree v1.4.1
	github.com/gabriel-vasile/mimetype v1.4.8
	github.com/google/uuid v1.6.0
	github.com/pborman/getopt/v2 v2.1.0
	github.com/xmppo/go-xmpp v0.2.10
	golang.org/x/crypto v0.32.0
	salsa.debian.org/mdosch/xmppsrv v0.3.3
)

require (
	github.com/ProtonMail/go-crypto v1.1.5 // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/cloudflare/circl v1.5.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
